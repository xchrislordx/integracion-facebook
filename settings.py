import os
from dotenv import load_dotenv


env = load_dotenv
#intanciamos los tokens usando mayusculas
ACCESS_TOKEN = os.getenv("ACCESS_TOKEN","-")
VERIFY_TOKEN = os.getenv("VERIFY_TOKEN","-")