from flask import Flask, render_template, request
from settings import ACCESS_TOKEN,VERIFY_TOKEN
app = Flask(__name__)


@app.route('/')
def index():
    if(request.args.get("hub.verify_token","") == VERIFY_TOKEN):
        print("Verificado")
        return request.args.get("hub.challenge","")
    else:
        print("token equivocado")
        return "Error, validación de token equivocado"

if __name__ == '__main__':
  app.run( port=8000, debug=True)
